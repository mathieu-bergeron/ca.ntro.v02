# Singleton Vs Collection

Pour débuter:

1. `ModelePartie` : singleton
1. `ModelePartie[]` : toute la collection
1. `ModelePartie[id]` : un seule instance parmi la collection, identifiée par son `id`

Plus tard:

1. Un petit langage permettant de filtrer les instances
    * (il faut que ça fonctionne avec l'idée de verrou)


# `modified(MonModele.partie)` est une condition qui reçoit `MsgObservation`

# Sur le frontal: `.observes(ModelePartie.class)`, un racourci

* un raccourci pour `-- MsgObservation<ModelePartie> --> modified<ModelePartie.class>`

# Sur le dorsaA: `.modifies(ModelePartie.class)`, un racourci

# Watch du Json

* `-- MsgJsonModified --> modified<JsonFile>`



## Idée

Afficher une capture `.png` du bouton qui envoi un `Evt` pour illustrer que le `Evt` vient du bouton

