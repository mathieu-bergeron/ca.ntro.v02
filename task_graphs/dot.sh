for i in *.dot; 
do
    target="$i".tmp
    cat $i | sed "s/}[[:space:]]\+/}/g" | sed 's/\xc2\xa0//g'>  $target
    dot -Tpng $target > $(echo $i | sed "s/dot/png/"); 
    rm $target
done
