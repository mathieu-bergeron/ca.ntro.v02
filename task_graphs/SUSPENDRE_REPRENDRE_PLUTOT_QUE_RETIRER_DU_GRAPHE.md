## Passer à suspendre/reprendre plutôt que retirer du graphe

```java
    private static Task premiereTache;

    // ...


        premiereTache = creerDonneesPartie(tasks);


    public static void suspendreAffichagePartie(){
        premiereTache.suspend();
    }

    public static void reprendreAffichagePartie(){
        premiereTache.resume();
    }
```

## NE PLUS SUPPORTER LE RETRAIT DU GRAPHE

1. Ce n'est pas plus clair que suspendre
    * c'est plus clair de conserver les tâches dans le graphe et de les mettres en gris (ou opacité 0.5)
      pour signifier qu'elles sont suspendues

1. Ça fait émerger des complications
