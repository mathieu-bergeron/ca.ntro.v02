## Ajouter le id à un groupe de tâches ajoute automatiquement à toutes les sous-tâches

si on fait

```java
tasks.taskGroup("AfficherPartie", id("alice")) // le id("alice") est la cible (target) du groupe de tâche (et de toutes les sous-tâches)

     .contains(subTasks -> {

            // le id("alice") est par défaut dans tout waitsFor comme
            // p.ex waitsFor(modified(ModelePartie.class)) 

            //...
     })
```

alors chaque sous-tâche est avec le modelMatcher(id("alice")) par défaut

## Changer le modelId en cours d'exécution

```java
TaskGroup afficherPartie = 

          tasks.taskGroup("AfficherPartie", id("alice"))

               .contains(subTasks -> {



               }).getTask();

// plus tard, en cours d'exécution

// on conserve les tâches, mais on change la cible par défaut
afficherPartie.changeTarget(id("bob"));
```

## Insister pour cibler le Singleton

Si on ajoute le concept de cible par défaut, il faut une méthode pour insister qu'on veut cibler le singleton

```java
TaskGroup afficherPartie = 

          tasks.taskGroup("AfficherPartie", id("alice"))

               .contains(subTasks -> {

                   subTasks.task("ajouterRendezVous")

                           .maySend(MsgAjouterRendezVous, singleton()) // on insite qu'on veut cibler le singleton

                           .executes(inputs, outputs){

                                // ...

                           }

               }).getTask();
```





