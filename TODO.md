# TODO

Dans l'ordre, on doit:

1. Graphe de tâches simple + afficher/animer le graphe de tâches (dans un World2dFx)
    * il faudra récupérer le layout de GraphViz
    * visualiser plutôt avec https://graphstream-project.org/ ??? 

1. Finaliser la sémantique du graphe de tâches


## Graphe de tâches

1. Il faut commencer par finaliser la sémantique et l'implantation du graphe de tâches

1. Tout va se faire via le graphe de tâches, alors ça doit fonctionner!
    * initialiser Ntro
    * re-charger le code après modification (via un ClassLoader spécifique à Ntro)
    * re-charger les modèles après la modification d'un fichier JSON
        * NOTE: 
            * se servir de graphe pour synchroniser la lecture/écriture des fichiers JSON
            * le graphe permet une seule opération à la fois
            * quand on est en opération d'écriture, on ne pourra pas déclencher une lecture (et vice-versa)
            * sur watch("MonModele.json"), on ne lit pas le fichier tout de suite, on ajoute une tâche à la liste des tâches

    * recharger les vues après 
    * re-lancer les tâches

## Faire fonctionner plusieurs graphes de tâches

1. On a trois graphes de tâches:
    * le Frontend de l'application usager (déjà créé lors de l'initialisation)
    * le Backend de l'application usager
    * le graphe Ntro
        * pour envoyer des messages Ntro (comme avertir le serveur qu'on a un nouveau modified dans le graphe)
        * pour charger les modèles, les vues, etc.

1. Chaque graphe est sur un thread séparé
    * communication uniquement par messages entre les graphes de tâche

1. Un graphe de tâches est sur un seul thread
    * pas besoin de verrou pour les modèles sur le graphe du Backend

## Format de message: pas besoin de rester avec JSON

1. On peut utiliser JSON uniquement pour IO avec usager (fichiers et/ou afficher les messages)

1. Pour la «vraie» communication, on peut envoyer du binaire (flux d'objets Java)


## Pas besoin de générer les getter/setter

1. On devrait être capable d'aller chercher l'info comme JavaFX le fait pour les @FXML

## Écrire en même temps un guide de style


## App compagnon

1. Le développement de l'App compagnon pourrait se faire en Ntro aussi
