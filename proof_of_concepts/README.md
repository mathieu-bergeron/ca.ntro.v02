# Preuves de concept

1. JDI + Fx: exécuter une application Fx complétement dans le débogueur 
    * deux fenêtres: une de l'App + une pour l'App compagnon

1. Dot + Fx: afficher un graphe dont le layout a été fait par Dot
    * layout adaptatif pour les graphes d'objets (p.ex. forcer la flèche par derrière pour precedent d'une liste chaînée)

1. App compagnon comme serveur

1. Extraire les attributs sans getter/setter (repiquer comment gson ou JavaFx le fait... avec Unsafe?)

1. Compilation en continu avec Gradle

1. Recharger les vues FXML: WindowFx avec une VueRacine. Simuler un petit graphe de tâches.

1. Utiliser Vertx et les Sock.JS plutôt que WebSocket

1. Version Web (ajout d'un DelayedBackendTasks pour les effets secondaires à effectuer après que la requête ait répondue)

1. JSweet

1. Un Runner pour Python plutôt que Java
    * mais communication avec la même App compagnon (i.e. affichage du graphe d'objets)

## Librairie pour le layout

TODO: chercher avec diagramme

... difficle à trouver

1. jFiles est propriétaire
1. jgrapht ne fait pas le hierarchique
1. graphstream ne fait pas le hierarchique
1. gephi a abandonné le hierarchique à partir de la version 0.9
    * on pourrait piller le code pour retrouver uniquement le code de layout qui s'applique au hierarchique

1. ?? https://github.com/brunomnsilva/JavaFXSmartGraph
    * layout de type ressort

En JS?

1. GoJS... propriétaire
