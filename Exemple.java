package mon_projet.exemple;  // doit être dans un paquet
                             // ne jamais utiliser Ntro ou ntro dans les noms
                            
import ca.ntro.*;                          // paquet racine contient toutes les classes de l'API
import static ca.ntro.BackendTasks.*;
import static ca.ntro.FrontendTasks.*;

public class Exemple {

    public static void main(String[] args) {
        // Ntro n'est pas encore initialisé, on peut uniquement appelé Ntro.launch() et Ntro.options()


        // ne pas appeler si on veut les options par défaut
        // (on peut aussi mettre les options dans ".ntro/config.json")
        Ntro.options()
            .runStaticAnalysis()         // analyse statique du code pour détecter des erreurs spécifiques à Ntro
            .connectToGradle()           // connexion à la compilation en continu de Gradle
            .connectToInspector()        // connexion à l'App compagnon
            .runInDebugger();            // exécuter le code dans une autre VM, dans le débogueur


        Ntro.launch(args);
    }

    public static void ntroMain(String[] args){
        // Ntro est initialisé, on peut appeler n'importe quelle méthode de Ntro
        
       
        monCommun();         // enseigner aux étudiants à graduellement extraire le code quand ça devient trop gros

        monDorsal();

        monFrontal();
    }

    private static void mesImplantations(){

        NtroCore.classes()
                .registerImplementation(VuePartie.class, VuePartieFx.class);  // noter qu'il faut instancier VuePartieFx.class là où VuePartie.class est déclarée
                                                                              //
                                                                              // NtroCore pour les ca.ntro.core (et les services que les étudiant.es peuvent ignorer)
    }

    private static void monCommun(){
        // détour par Ntro.models() pour laisser de la place à ce que l'interface des modèles soit différents sur d'autre plateforme?

        Ntro.models()
            .registerModel(ModelePartie.class);    // on pourrait détecter dans le code, mais ça me semble plus pédagogique de «forcer» à déclarer

        Ntro.models()
            .registerModel(ModeleFileAttente.class)  
            .options()
            .watchJsonFile();  // mettre un watch sur le fichier .json et recharger le modèle si le fichier change
                               

        Ntro.messages()
            .registerMessage(MsgAjouterPoint.class);                   
    }

    private static void monFrontal(){

        Ntro.views()
            .createViewLoader(VuePartie.class, "/partie.xml");  // détour par Ntro.views() parce que les Vues c'est spécifique à la plateforme et on pourrait avoir une version de Ntro pour le Web ou pour Swing 
                                                                // NOTE: Ntro.views() est ignoré sur le serveur (en mode sans affichage)
                                                                                     
        Ntro.views()
            .createViewLoader(VueFileAttente.class, "/file_attente.xml")
            .options()
            .watchXmlFile();    // mettre un watch sur le fichier .xml et recharger la Vue si le fichier xml est modifié (sans avoir à relancer l'application)
                                                                                     
                                                                                     
        Ntro.views().registerCss("/prod.css");
        Ntro.views().registerTranslations(Ntro.locale("fr"), "/chaine_fr.properties");      
        Ntro.views().registerTranslations(Ntro.locale("en"), "/chaine_en.properties");      


        FrontendTasks frontendTasks = Ntro.frontendTasks();       // le graphe exécute déjà
                                                                  // dès qu'une tâche est ajoutée, elle est dans la file de tâches à exécuter
                                                                 
        frontendTasks.task("afficherVuePartie")                   // tout est vérifié dès l'ajout de la tâche

                     .waitsFor(window())                          // message d'erreur dès la création de la tâche s'il y a un problème

                     .waitsFor(viewLoader(VuePartie.class))       // la tâche exécute dès que possible (le graphe "roule" déjà)

                     .executesAndReturns(inputs -> {

                         Window                window           = inputs.get(window());                             // le Window contient déjà la VueRacine
                         ViewLoader<VuePartie> viewLoaderPartie = inputs.get(viewLoader(VuePartie.class));

                         VuePartie vuePartie = viewLoaderPartie.createView();

                         window.setMainView(vuePartie);

                         return vuePartie;

                     });


        Ntro.frontendTasks().task("afficherPatie")               // il y a un seul graphe de tâche du frontal, on peut l'appeler directement
                                                                 // pas besoin de le sauvegarder dans une variable
                                                                
                            .waitsFor("afficherVuePartie")            // autant de waitsFor que requis

                            .reactsTo(modified(ModelePartie.class))   // un seul reactsTo par tâche
								     
			    .triggers(event(EvtActionJoueur.class))        // NOUVEAU: doit déclarer les outputs
			    .sends(message(MsgPropagerActionJoueur.class)) // NOUVEAU

                            .executes((inputs, outputs) -> {

                                VuePartie vuePartie                   = inputs.get("afficherVuePartie");
                                Modified<ModelePartie> modifiedPartie = inputs.get(modified(ModelePartie.class));

                                modelePartie.current().afficherSur(vuePartie);

				EvtActionJoueur         evtActionJoueur         = outputs.get(event(EvtActionJoueur.class));
				MsgPropagerActionJoueur msgPropagerActionJoueur = outputs.get(message(MsgPropagerActionJoueur.class));

				msgPropagerActionJoueur.send();

                            });
    }

    private static void monDorsal(){

        BackendTasks backendTasks = Ntro.backendTasks();

        backendTasks.task("ajouterPoint")

		    .watisFor(model(ModelePartie.class))             

                    .reactsTo(message(MsgAjouterPoint.class))

		    .maySend(modification(ModelePartie.class))             // NOUVEAU:déclare le modified

                    .executes((inputs, outputs) -> {

                        ModelePartie    modelePartie    = inputs.get(model(ModelePartie.class));
                        MsgAjouterPoint msgAjouterPoint = inputs.get(message(MsgAjouterPoint.class));

			Modification<ModelePartie> modification = outputs.get(modification(ModelePartie.class));

                        msgAjouterPoint.appliquerA(modelePartie);

			// ??? plutôt que automatique ???
			modification.setModel(modelePartie);
			modification.sendNotificationToFrontend();
                    });
    }

}
