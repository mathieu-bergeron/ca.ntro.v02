# Reconception Ntro

## Pour utiliser Ntro

```bash
$ sh gradlew mon_projet   # compilation en continu
```

```java
public static void main(String[] args){

    Ntro.launch(args, Ntro.options.enableInspector()
                                  .enableDebug());
}
```

## Architecture


* une *App compagnon* qui visualise tout, y compris 
    * la recompilation du code
    * le chargement du code (via le *ClassLoader* Ntro)

* un *Runner* qui exécute le code étudiant dans une autre VM en mode débogeur
    * copie le graphe d'objet dans des fichiers .json
    * envoi des messages à l'App compagnon pour la visualisation

## Fonctionnalités: visualiser et animer tout ce qu'on peut

* la chronologie de l'application: le code qui s'exécute et dans quel ordre

* visualiser/animer le code simple

* animer le graphe d'objets

* animer le/les graphe des tâches
    * si on a 2 graphe des tâches, on a 2 chronologie
    * NOTE: 
        * on ne veut pas d'un graph de tâche multi-thread
        * plus pédagogique d'avoir un graphe

* animer l'envoi de messages et d'événements

* détecter et animer les modifications à un graphe d'objets
    * NOTE: c'est log(N^2) pour les listes, alors ne jamais faire en prod ou sur un gros modèle!

* manipuler le graphe des tâches directement dans l'App compagnon

## Support pour d'autres langages

* il faut implanter un autre *Runner*, qui va se connecter à l'App compagnon




